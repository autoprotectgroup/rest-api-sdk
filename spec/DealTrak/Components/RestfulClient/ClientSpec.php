<?php

namespace spec\DealTrak\Components\RestfulClient;

use DealTrak\Components\RestfulClient\Client;
use DealTrak\Components\RestfulClient\HttpClientInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;

class ClientSpec extends ObjectBehavior
{
    function let(
        HttpClientInterface $httpClient,
        ResponseInterface $response
    ) {
        $this->beConstructedWith($httpClient, [], '');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Client::class);
    }

    function it_should_error_during_invalid_pathmap()
    {
        $this->shouldThrow(\Exception::class)->during('resource', ["clientelle"]);
    }

    function it_should_allow_node_transversal(
        HttpClientInterface $httpClient
    ) {
        $this->beConstructedWith(
            $httpClient,
            [
                'roger' => [
                    'path' => 'rabbit'
                ]
            ],
            'base'
        );
        $result = $this->resource('roger');
        $result->shouldBeAnInstanceOf(Client::class);
        $result->getPath()->shouldBe('base/rabbit');
    }

    function it_should_append_identifiers(
        HttpClientInterface $httpClient
    ) {
        $this->beConstructedWith(
            $httpClient,
            [
                'roger' => [
                    'path' => 'rabbit'
                ]
            ],
            'base'
        );
        $result = $this->resource('roger', 123);
        $result->shouldBeAnInstanceOf(Client::class);
        $result->getPath()->shouldBe('base/rabbit/123');
    }

    function it_should_append_multiple_nodes(
        HttpClientInterface $httpClient
    ) {
        $this->beConstructedWith(
            $httpClient,
            [
                'roger' => [
                    'path' => 'rabbit'
                ],
                'foo' => [
                    'path' => 'bar'
                ]
            ],
            'base'
        );
        $result = $this->resource('roger', 123)->resource('foo', 123);
        $result->shouldBeAnInstanceOf(Client::class);
        $result->getPath()->shouldBe('base/rabbit/123/bar/123');
    }

    function it_should_perform_get(
        HttpClientInterface $httpClient,
        $response
    ) {
        $httpClient->get('rabbit', ['fly' => 'nose'])->shouldBeCalled();
        $httpClient->get('rabbit', ['fly' => 'nose'])->willReturn($response);

        $this->beConstructedWith(
            $httpClient,
            [
                'peter' => [
                    'path' => 'rabbit'
                ]
            ]
        );
        $this->resource('peter')->get(['fly' => 'nose'])->shouldBe($response);
    }

    function it_should_perform_delete(
        HttpClientInterface $httpClient,
        $response
    ) {
        $httpClient->delete('rabbit', [])->shouldBeCalled();
        $httpClient->delete('rabbit', [])->willReturn($response);

        $this->beConstructedWith(
            $httpClient,
            [
                'peter' => [
                    'path' => 'rabbit'
                ]
            ]
        );
        $this->resource('peter')->delete()->shouldBe($response);
    }

    function it_should_perform_post(
        HttpClientInterface $httpClient,
        $response
    ) {
        $httpClient->post('rabbit', [])->shouldBeCalled();
        $httpClient->post('rabbit', [])->willReturn($response);

        $this->beConstructedWith(
            $httpClient,
            [
                'peter' => [
                    'path' => 'rabbit'
                ]
            ]
        );
        $this->resource('peter')->post()->shouldBe($response);
    }

    function it_should_perform_patch(
        HttpClientInterface $httpClient,
        $response
    ) {
        $httpClient->patch('rabbit', ['change' => true])->shouldBeCalled();
        $httpClient->patch('rabbit', ['change' => true])->willReturn($response);

        $this->beConstructedWith(
            $httpClient,
            [
                'peter' => [
                    'path' => 'rabbit'
                ]
            ]
        );
        $this->resource('peter')->patch(['change' => true])->shouldBe($response);
    }
}
