# Restful API SDK

## Example Code
```
<?php
// Load the Autoloader (Composer)
include __DIR__.'/vendor/autoload.php';

// Create your entities to path mapping (all at the top-level)
$pathMap = [
    'clients' => [
        'path' => 'clients',
    ],
    'transactionAggregates' => [
        'path' => 'transaction-aggregates',
    ],
    'reconciliations' => [
        'path' => 'reconciliations',
    ],
    'transactions' => [
        'path' => 'transactions',
    ],
];

// Create a Guzzle CLient
$guzzle = new GuzzleHttp\Client([
    // Set the base URI to the Restful endpoint (include path if needs be)
    'base_uri' => 'https://13.37.13.37:8000/web/index.php/',
    
    // Add any other Guzzle Options
    'timeout'  => 2.0,
    'headers'  => [
        // Add any other required options, for example api-key
        // is required for Transactional Billing API
        'api-key' => 'wizard',
        // Also this header must be set for the aforementioned API
        'content-type' => 'application/json'
    ]
]);

// Create a new HttpClient for the RestfulClient SDK, Guzzle is provided but
// others can be created by implementing the HttpClientInterface interface.
// Optionally add data and error message array index keys (if your data
// isn't at the top level).
$httpClient = new DealTrak\Packages\RestfulClient\HttpClient\Guzzle(
    $guzzle
);

// Create the Restful API SDK injecting the HttpClient and the pathMap
$client = new DealTrak\Packages\RestfulClient\Client($httpClient, $pathMap);

// Make some Requests
// GET /clients/2/transactions
$result = $client->resource('clients',2)->resource('transactions')->get();
var_dump($result);

// GET /clients/2/transaction-aggregates/6
$result = $client->resource('clients', 2)->resource('transactionAggregates', 6)->get();
var_dump($result);

// POST /clients
$result = $client->resource('clients')->post(['name' => 'New Client', 'code' => 'client123']);
var_dump($result);

// DELETE /clients/8
$result = $client->resource('clients', 8)->delete();
var_dump($result);

// This will fail as roles doesn't exist in the pathMap
$result = $client->resource('clients', 2)->resource('roles')->get();
```

## Optimize
Under the hood, a new object is created for `resource` call. In other words every call to the client that is not a `get`, `patch`, `post` or `delete` creates a new client. 
So if we have `$client->get()` then we have one Client SDK object, if we have `$client->resource('clients', 123)->delete()` then we have two and finally if we have `$client->resource('clients', 123)->resource('transactions', 123)->resource('refunds')->get()` then we have 4. 
This isn't to bad but if we put it in a loop it could become a problem, see the examples below on good and bad ways.
In the future we could add caching so that it returns a previous object if they'd be duplicates of each other but this increases complexity so not yet included, if it ever will...
```
// Do not do this
foreach($newTransactionsToMake as $newTransactionToMake) {
    $client->resource('clients')->resource('transactions')->post($newClientToMake);
}

// Do this
$transactionsClient = $client->resource('clients')->resource('transactions');
foreach($newTransactionsToMake as $newTransactionToMake) {
    $transactionsClient->post($newClientToMake);
}
```

