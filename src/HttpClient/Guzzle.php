<?php

namespace DealTrak\Components\RestfulClient\HttpClient;

use GuzzleHttp\Client as GuzzleClient;
use DealTrak\Components\RestfulClient\HttpClientInterface;
use Psr\Http\Message\ResponseInterface;

class Guzzle implements HttpClientInterface
{
    /**
     * @var GuzzleClient
     */
    private $client;

    /**
     * @param GuzzleClient $client
     *
     * @return self
     */
    public function __construct(
        GuzzleClient $client
    ) {
        $this->client = $client;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get(
        $path,
        array $data = []
    ) {
        return $this->request('GET', $path, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        $path,
        array $data = []
    ) {
        return $this->request('DELETE', $path, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function patch(
        $path,
        array $data = []
    ) {
        return $this->request('PATCH', $path, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function post(
        $path,
        array $data = []
    ) {
        return $this->request('POST', $path, $data);
    }

    /**
     * @param string  $method
     * @param string  $path
     * @param mixed[] $data
     *
     * @return ResponseInterface
     */
    private function request($method, $path, array $data)
    {
        // Make the Request ($data is querystring for GET otherwise json body)
        /** @var ResponseInterface $response */
        $response = $this->client->request(
            $method,
            $path,
            [$method === 'GET' ? 'query' : 'json' => $data]
        );

        return $response;
    }
}
