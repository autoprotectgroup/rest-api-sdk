<?php

namespace DealTrak\Components\RestfulClient;

interface HttpClientInterface
{
    /**
     * Make a GET request to $path with $data.
     *
     * @param string  $path
     * @param mixed[] $data
     *
     * @return mixed
     */
    public function get($path, array $data = []);

    /**
     * Make a GET request to $path with $data.
     *
     * @param string  $path
     * @param mixed[] $data
     *
     * @return mixed
     */
    public function post($path, array $data = []);

    /**
     * Make a GET request to $path with $data.
     *
     * @param string  $path
     * @param mixed[] $data
     *
     * @return mixed
     */
    public function delete($path, array $data = []);

    /**
     * Make a GET request to $path with $data.
     *
     * @param string  $path
     * @param mixed[] $data
     *
     * @return mixed
     */
    public function patch($path, array $data = []);
}
