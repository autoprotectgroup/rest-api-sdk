<?php

namespace DealTrak\Components\RestfulClient;

use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var mixed[]
     */
    private $pathMap;

    /**
     * @var string
     */
    private $path;

    /**
     * @param HttpClientInterface $httpClient
     * @param array[]             $pathMap
     * @param string              $path
     *
     * @return self
     */
    public function __construct(
        HttpClientInterface $httpClient,
        array $pathMap = [],
        $path = ''
    ) {
        $this->httpClient = $httpClient;
        $this->pathMap = $pathMap;
        $this->path = $path;

        return $this;
    }

    /**
     * Returns a new instance of ourself with the path extend based on call and
     * arguments. `->resource('something', 123)`` will append `/something/123` to the path.
     *
     * @param string $resource
     * @param string|int $id
     * @return Client
     */
    public function resource($resource, $id = null)
    {
        if (!isset($this->pathMap[$resource])) {
            throw new \BadMethodCallException($resource.' does not exist');
        }
        $path = $this->pathMap[$resource]['path'];
        if ($id) {
            $path .= '/'.$id;
        }
        
        return new self(
            $this->httpClient,
            $this->pathMap,
            $this->path.'/'.$path
        );
    }

    /**
     * Gets the current path.
     *
     * @return string
     */
    public function getPath()
    {
        return ltrim($this->path, '/');
    }

    /**
     * Calls GET with the path with given data.
     *
     * @param string[]|int[]
     *
     * @return ResponseInterface
     */
    public function get(array $data = [])
    {
        return $this->httpClient->get($this->getPath(), $data);
    }

    /**
     * Calls DELETE with the path with given data.
     *
     * @param mixed[]
     *
     * @return ResponseInterface
     */
    public function delete(array $data = [])
    {
        return $this->httpClient->delete($this->getPath(), $data);
    }

    /**
     * Calls PATCH with the path with given data.
     *
     * @param mixed[]
     *
     * @return ResponseInterface
     */
    public function patch(array $data = [])
    {
        return $this->httpClient->patch($this->getPath(), $data);
    }

    /**
     * Calls POST with the path with given data.
     *
     * @param mixed[]
     *
     * @return ResponseInterface
     */
    public function post(array $data = [])
    {
        return $this->httpClient->post($this->getPath(), $data);
    }
}
